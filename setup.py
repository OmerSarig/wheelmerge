import os

from setuptools import setup, find_packages

setup(
    name="wheelmerge",
    description="Utility to merge wheels to executable zip archives",
    url="https://gitlab.com/OmerSarig/wheelmerge",
    author="Omer Sarig",
    author_email="omer@sarig.co.il",
    packages=find_packages(),
    python_requires=">=2.7",
    install_requires=[],
    entry_points={
        "console_scripts": [
            " wheelmerge = wheelmerge:main"
        ]
    }
)
