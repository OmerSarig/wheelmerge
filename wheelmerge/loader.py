import base64
import os
import sys
import tempfile


zip_content = """{}"""
with tempfile.NamedTemporaryFile("wb", suffix=".zip") as zip_file:
    zip_file.write(base64.decodestring(zip_content))
    os.environ["SELF_DESTRUCT"] = "True"
    os.execve(sys.executable, [zip_file.name] + sys.argv[1:], os.environ)
