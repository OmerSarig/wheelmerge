import atexit
import sys
import re
import os

import entrypoints


def main():
    entry_points = entrypoints.get_group_all("console_scripts", sys.path[:2])

    if bool(os.environ.pop("SELF_DESTRUCT", False)):
        def self_destruct_handler(running_file):
            os.remove(running_file)
        atexit.register(self_destruct_handler, sys.argv[0])

    if len(sys.argv) == 1:
        print("Scripts: {}".format(", ".join(entry_point.name for entry_point in entry_points)))
    elif len(sys.argv) > 1:
        sys.argv = sys.argv[1:]
        sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
        for entry_point in entry_points:
            if entry_point.name == sys.argv[0]:
                return entry_point.load()()
        else:
            sys.stderr.write("No such console script '{}'\n".format(sys.argv[0]))


if __name__ == '__main__':
    sys.exit(main())
